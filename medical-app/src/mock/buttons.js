const buttonsArray = [
  {
    text: 'Препарат 1',
    drug: 'left',
    color: 'violet',
  },
  {
    text: 'Препарат 2',
    drug: 'top',
    color: 'blue',
  },
  {
    text: 'Препарат 3',
    drug: 'right',
    color: 'yellow',
  },
];
export default buttonsArray;
