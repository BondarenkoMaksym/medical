const drugsCounter = [
  {
    icon: 'sad.svg',
    drugColor: 'getVioletDrug',
    index: 1,
  },
  {
    icon: 'happy.svg',
    drugColor: 'getBlueDrug',
    index: 2,
  },
  {
    icon: 'heart.svg',
    drugColor: 'getYellowDrug',
    index: 3,
  },
];
export default drugsCounter;
