import Vue from 'vue';
import VueRouter from 'vue-router';
import StartPage from '../views/StartPage.vue';
import Final from '../views/Final.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'StartPage',
    component: StartPage,
  },
  {
    path: '/drugsale',
    name: 'DrugSale',
    component: () => import(/* webpackChunkName: "about" */ '../views/DrugSale.vue'),
  },
  {
    path: '/final',
    name: 'Final',
    component: Final,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
