import Vue from 'vue';
import Vuex from 'vuex';

import patientsArray from '../mock/patients';
import buttonsArray from '../mock/buttons';
import drugsCounter from '../mock/drugsCounter';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    patientNumber: 0,
    drugNumber: '',
    drugs: {
      drugViolet: 0,
      drugBlue: 0,
      drugYellow: 0,
    },
    currentStyle: 0,
    patientsArray,
    buttonsArray,
    drugsCounter,
  },
  mutations: {
    setPatientNumber(state) {
      state.patientNumber++;
    },
    resetPatientNumber(state) {
      state.patientNumber = 0;
      // eslint-disable-next-line guard-for-in
      for (const drug in state.drugs) {
        state.drugs[drug] = 0;
      }
    },
    setNewStyle(state) {
      state.currentStyle++;
    },
    setDrugNumber(state, payload) {
      state.drugNumber = payload;
      switch (payload) {
        case 'left':
          return state.drugs.drugViolet++;
        case 'top':
          return state.drugs.drugBlue++;
        case 'right':
          return state.drugs.drugYellow++;
        default:
          return '';
      }
    },
  },
  actions: {
    increasePatientNumber({ commit }) {
      commit('setPatientNumber');
    },
    resetPatientNumber({ commit }) {
      commit('resetPatientNumber');
    },
    refreshStyle({ commit }) {
      commit('setNewStyle');
    },
    changeDrugNumber({ commit }, payload) {
      commit('setDrugNumber', payload);
    },
  },
  getters: {
    getPatients(state) {
      return state.patientsArray;
    },
    getButtons(state) {
      return state.buttonsArray;
    },
    getPatientNumber(state) {
      return state.patientNumber;
    },
    getDrugColor(state) {
      return state.drugNumber;
    },
    getStyleNumber(state) {
      return state.currentStyle;
    },
    getDrugsCounter(state) {
      return state.drugsCounter;
    },
    getAllDrugsCount(state) {
      return state.drugs;
    },
  },
});
